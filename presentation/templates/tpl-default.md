<% content %>

<grid drag="100 6" drop="top" flow="row" justify-content="space-between" filter="saturate(0.1)" style="font-size: 1.5rem; color: grey;">
<%? section %> <!-- element style="text-align:right;font-size: 0.8rem" -->
</grid>
